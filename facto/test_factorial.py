import unittest
from factorial import solve

class TestFactorial(unittest.TestCase):
    def test_1(self):
        assert solve(4) == 24 # test when 4 is given as input the output is 24.
        assert solve(5) == 120 # test when 5 is given as input the output is 120.
        assert solve(0) == 1

    def test_2(self):
        assert not solve(5) == 5
        # assert not solve(2) == 2 // this test case will fail

    def test_3(self):
        assert solve(-5) == 0



if __name__ == '__main__':
    unittest.main()

