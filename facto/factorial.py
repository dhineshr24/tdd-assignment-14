
def solve(number):
    if number < 0:
        return 0
    if number == 0:
        return 1
    else:
        return number * solve(number-1)